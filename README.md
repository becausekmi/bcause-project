# README #

A project funded by the Office of Naval Research of the US and led by the IDEA Group of The Open University, to design the next generation of discussion-enhanced collective intelligence and online deliberation platform.


## License

The BCAUSE project is licensed under the
[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html), also
included in our repository in the `COPYING` file.
