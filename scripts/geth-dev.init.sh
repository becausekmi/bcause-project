#!/usr/bin/env sh

cd $(dirname "$0")/..
PROJECT_DIR=$(pwd)
# echo $PROJECT_DIR

# VERS=stable
VERS=latest

# Init ethereum bootnode
echo "Init dev geth-node0"
docker run --rm \
  -v $PROJECT_DIR/geth/node0/data:/data \
  -v $PROJECT_DIR/geth/node0/node.key:/node.key \
  -v $PROJECT_DIR/geth/bcause-dev.json:/genesis.json \
  ethereum/client-go:alltools-${VERS} \
  geth \
    --datadir /data \
    --nodekey node.key \
    init genesis.json
