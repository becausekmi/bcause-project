#!/usr/bin/env sh

docker run \
  --detach \
  --network=bcause-project_default \
  --name=bcause-project-ganache \
  --publish 8545:8545 \
  trufflesuite/ganache-cli:latest \
  --accounts 3
