#!/usr/bin/env sh

cd $(dirname "$0")/..
PROJECT_DIR=$(pwd)
# echo $PROJECT_DIR

echo "THIS IS A WORKING PROGRESS"
echo "Before run, check this script. Then remove these lines."
exit 0

# @todo think another strategy for workspace
# - Generate boot.key
# - Generate genesis.json

# Generate all keys and addresses
# docker run --rm \
#   -v $(pwd)/signer01/data:/data \
#   -v $(pwd)/signer01/vault:/tmp \
#   ethereum/client-go:alltools-latest \
#   geth \
#     --datadir /data \
#     --password /tmp/password \
#     account new

# Generate genesis (interative) and copy container dirs
# @todo (ATTENTION: puppet doen't close)
# the address will be found in data/keystore/UTC-...

# VERS=stable
VERS=latest

# Init ethereum bootnode
echo "Init bootnode"
docker run --rm \
  -v $(pwd)/geth/bootnode/data:/data \
  -v $(pwd)/geth/bootnode/node.key:/node.key \
  -v $(pwd)/geth/bcause.json:/genesis.json \
  ethereum/client-go:alltools-${VERS} \
  geth \
    --datadir /data \
    --nodekey node.key \
    init genesis.json

# Init ethereum signer(s)
echo "Init signer"
docker run --rm \
  -v $(pwd)/geth/signer/data:/data \
  -v $(pwd)/geth/bcause.json:/genesis.json \
  ethereum/client-go:alltools-${VERS} \
  geth \
    --datadir /data \
    init genesis.json

# Init ethereum endpoint(s)
echo "Init endpoint"
docker run --rm \
  -v $(pwd)/geth/endpoint/data:/data \
  -v $(pwd)/geth/bcause.json:/genesis.json \
  ethereum/client-go:alltools-${VERS} \
  geth \
    --datadir /data \
    init genesis.json

# @todo create default endpoint account


# Init ethereum client(s)
echo "Init client"
docker run --rm \
  -v $(pwd)/geth/client/data:/data \
  -v $(pwd)/geth/bcause.json:/genesis.json \
  ethereum/client-go:alltools-${VERS} \
  geth \
    --datadir /data \
    init genesis.json