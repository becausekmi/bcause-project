#!/usr/bin/env sh

cd "$(dirname "$0")/.."
PROJECT_DIR=$(pwd)
# echo $PROJECT_DIR

echo "THIS IS A WORKING PROGRESS"
echo "Before run, check this script. Then remove these lines."
exit 0

echo "Resetting bootnode"
rm -rf bootnode/data

echo "Resetting signer"
rm -rf signer/data/geth

echo "Resetting endpoint"
rm -rf endpoint/data/geth

echo "Resetting client"
rm -rf client/data/geth
